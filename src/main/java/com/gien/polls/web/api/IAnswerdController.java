package com.gien.polls.web.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface IAnswerdController {
	
	@RequestMapping(path="/prueba/answerd",
			consumes={"application/json"},
			produces={"application/json"},
			method=RequestMethod.POST)
	void addAnswerd();

}
