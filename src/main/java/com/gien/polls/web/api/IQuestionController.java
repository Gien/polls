package com.gien.polls.web.api;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gien.polls.domain.entities.QuestionEntity;

public interface IQuestionController {

	@RequestMapping(path="/question",
			consumes={"application/json"},
			produces={"application/json"},
			method=RequestMethod.POST)
	void addQuestion();

	@RequestMapping(path="/get/allquestion",
			consumes={"application/json"},
			produces={"application/json"},
			method=RequestMethod.GET)
	List<QuestionEntity> getAllQuestions();

	@RequestMapping(path="/{questionid}/get",
			consumes={"application/json"},
			produces={"application/json"},
			method=RequestMethod.GET)
	QuestionEntity getQuestionById(String questionid);
	
	

}
