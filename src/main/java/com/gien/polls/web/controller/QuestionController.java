package com.gien.polls.web.controller;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gien.polls.domain.entities.QuestionEntity;
import com.gien.polls.service.questions.IQuestionService;
import com.gien.polls.web.api.IQuestionController;

@RestController
@RequestMapping(path="questions")
public class QuestionController implements IQuestionController {
    private static final Logger log = LoggerFactory.getLogger(QuestionController.class);
	
	@Autowired
	IQuestionService questionsService;
	
	@Override
	public void addQuestion(){
		QuestionEntity question = new QuestionEntity()
				.withQuestionid(UUID.randomUUID().toString());
		questionsService.saveQuestion(question);
	}

	@Override
	public List<QuestionEntity> getAllQuestions() {
		return questionsService.getAllQuestions();
	}

	@Override
	public QuestionEntity getQuestionById(@PathVariable("questionid") String questionid) {
		return questionsService.getQuestionById(questionid);
	}
}
