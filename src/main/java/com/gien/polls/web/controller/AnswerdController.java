package com.gien.polls.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.gien.polls.domain.entities.AnswerdEntity;
import com.gien.polls.service.answeres.IAnswerdService;
import com.gien.polls.web.api.IAnswerdController;

@RestController
public class AnswerdController implements IAnswerdController {
    private static final Logger log = LoggerFactory.getLogger(AnswerdController.class);
	
	@Autowired
	IAnswerdService answerdService;
	
	@Override
	public void addAnswerd(){
		AnswerdEntity answerd = new AnswerdEntity();
		answerdService.save(answerd);
	}
}
