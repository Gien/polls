package com.gien.polls.enums;

public enum QuestionTypeEnum {
	
	TYPE1(1,"TIPO1");
	
	private int id;
	private String type;
	
	QuestionTypeEnum (int id, String type){
		this.id = id;
		this.type = type;
	}

}
