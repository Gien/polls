package com.gien.polls.enums;

public enum AuditTypeEnum {
	QUESTIONS("QUESTIONS");
	
	private String value;
	
	AuditTypeEnum (String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
