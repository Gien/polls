package com.gien.polls.enums;

public enum AuditActionTypeEnum {
	INSERT("QUESTIONS"),
	UPDATE("UPDATE"),
	DELETE("DELETE");
	
	private String value;
	
	AuditActionTypeEnum (String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
}
