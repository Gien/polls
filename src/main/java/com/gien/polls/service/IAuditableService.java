package com.gien.polls.service;

public interface IAuditableService<T> {

	T save(T objectAudit);

	T update(T objectAudit);

	void delete(T objectAudit);

}
