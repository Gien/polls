package com.gien.polls.service.questions;

import java.util.List;

import com.gien.polls.domain.entities.QuestionEntity;

public interface IQuestionService {

	QuestionEntity saveQuestion(QuestionEntity questions);

	List<QuestionEntity> getAllQuestions();

	QuestionEntity getQuestionById(String questionid);

}
