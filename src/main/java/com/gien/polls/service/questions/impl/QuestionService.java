package com.gien.polls.service.questions.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.gien.polls.domain.entities.QuestionEntity;
import com.gien.polls.domain.repositories.IQuestionRepository;
import com.gien.polls.service.IAuditService;
import com.gien.polls.service.impl.AuditableService;
import com.gien.polls.service.questions.IQuestionService;

@Component
public class QuestionService extends AuditableService implements IQuestionService {
    private static final Logger log = LoggerFactory.getLogger(QuestionService.class);
	
	@Autowired
	IQuestionRepository questionRepository;
	
	@Autowired
	IAuditService auditService;
	
	@Override
	public QuestionEntity saveQuestion(QuestionEntity questions){
		return (QuestionEntity) save(questions);
	}
	
	@Override
	public List<QuestionEntity> getAllQuestions(){
		return getRepository().findAll();
	}
	
	@Override
	public QuestionEntity getQuestionById(String questionid){
		return ((IQuestionRepository) getRepository()).findOneByQuestionid(questionid);
	}

	@Override
	protected JpaRepository getRepository() {
		return questionRepository;
	}

}
