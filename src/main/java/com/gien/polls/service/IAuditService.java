package com.gien.polls.service;

import com.gien.polls.domain.entities.BaseEntity;

public interface IAuditService {

	void save(BaseEntity object);

	void update(BaseEntity objectOld, BaseEntity objectNew);

	void delete(BaseEntity object);

}
