package com.gien.polls.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.gien.polls.domain.entities.BaseEntity;
import com.gien.polls.service.IAuditService;
import com.gien.polls.service.IAuditableService;

@Service
public abstract class AuditableService<T extends BaseEntity> implements  IAuditableService<T>{
    private static final Logger log = LoggerFactory.getLogger(AuditableService.class);
    
    @Autowired
    IAuditService auditService;

	@Override
	public final T save(T objectAudit) {
		
		if (getRepository().existsById(objectAudit.getId())){
			update(objectAudit);
		}else{
			getRepository().save(objectAudit);
		}
		
		auditService.save(objectAudit);
		return objectAudit;
	}

	@Override
	public final T update(T objectAudit) {
		
		Optional objectAuditOld = getRepository().findById(objectAudit); 
		
		getRepository().save(objectAudit);
		
		auditService.update((BaseEntity)objectAuditOld.get(), objectAudit);

		return objectAudit;
	}

	@Override
	public final void delete(T objectAudit) {
		objectAudit.setTsDelete();
		objectAudit.setUsDelete(1);
		
		getRepository().save(objectAudit);
		
		auditService.delete(objectAudit);
		
	}
	
	protected abstract JpaRepository getRepository();

}
