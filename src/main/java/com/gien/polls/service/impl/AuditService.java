package com.gien.polls.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gien.polls.domain.entities.AuditableEntity;
import com.gien.polls.domain.entities.BaseEntity;
import com.gien.polls.domain.entities.QuestionEntity;
import com.gien.polls.domain.repositories.IAuditRepository;
import com.gien.polls.enums.AuditActionTypeEnum;
import com.gien.polls.enums.AuditTypeEnum;
import com.gien.polls.service.IAuditService;

@Service	
public class AuditService implements IAuditService {
    private static final Logger log = LoggerFactory.getLogger(AuditService.class);
	
	@Autowired
	IAuditRepository auditRepository;
	
	@Override
	public final void save(BaseEntity object){
		log.info("objeto a autidat: {}", object.getClass());
		try {
			auditRepository.save(new AuditableEntity()
					.action(AuditActionTypeEnum.INSERT.getValue())
					.object(object.getClass().getName())
					.identify(object.getId())
					.values(object.toJson())
					.type(getType(object)));
		} catch (IOException e) {
			log.info("save Error: {}", e);
		}
	}
	
	@Override
	public final void update(BaseEntity objectOld, BaseEntity objectNew){
		log.info("objeto a autidat: {}", objectNew.getClass());
		try {
			auditRepository.save(new AuditableEntity()
					.action(AuditActionTypeEnum.UPDATE.getValue())
					.object(objectNew.getClass().getName())
					.identify(objectNew.getId())
					.values(objectNew.toJson())
					.changes(objectOld.toJson())
					.type(getType(objectNew)));
		} catch (IOException e) {
			log.info("save Error: {}", e);
		}
	}
	
	@Override
	public final void delete(BaseEntity object){
		log.info("objeto a auditar: {}", object.getClass());
		try {
			auditRepository.save(new AuditableEntity()
					.action(AuditActionTypeEnum.DELETE.getValue())
					.object(object.getClass().getName())
					.identify(object.getId())
					.values(object.toJson())
					.type(getType(object)));
		} catch (IOException e) {
			log.info("save Error: {}", e);
		}
	}
	
	private String getType(Object object){
		if (object instanceof QuestionEntity){
			return AuditTypeEnum.QUESTIONS.getValue();
		}else{
			return "";
		}
	}

}
