package com.gien.polls.service.answeres;

import com.gien.polls.domain.entities.AnswerdEntity;

public interface IAnswerdService {

	AnswerdEntity save(AnswerdEntity answeres);

}
