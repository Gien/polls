package com.gien.polls.service.answeres.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gien.polls.domain.entities.AnswerdEntity;
import com.gien.polls.domain.repositories.IAnswerdRepository;
import com.gien.polls.service.answeres.IAnswerdService;

@Component
public class AnswerdService implements IAnswerdService {
    private static final Logger log = LoggerFactory.getLogger(AnswerdService.class);
	
	@Autowired
	IAnswerdRepository answeresRepository;
	
	@Override
	public AnswerdEntity save(AnswerdEntity answeres){
		return answeresRepository.save(answeres);		
	}

}
