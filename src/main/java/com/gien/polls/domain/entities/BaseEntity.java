package com.gien.polls.domain.entities;

import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.databind.ObjectMapper;

@MappedSuperclass
public class BaseEntity implements Serializable{

    private static final long serialVersionUID = 1L;
    
	@Id
	@Column(name = "id")
    @GeneratedValue(generator="uuid")
    @GenericGenerator(name="uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	@Column(name = "ts_creado")
	private long tsCreate;
	
	@Column(name = "ts_editado")
	private long tsUpdate;
	
	@Column(name = "ts_borrado")
	private long tsDelete;
	
	@Column(name = "us_creado")
	private int usCreate;
	
	@Column(name = "us_editado")
	private Integer usUpdate;
	
	@Column(name = "us_borrado")
	private Integer usDelete;
	
	@PrePersist
	public void preInsert(){
		this.tsCreate = getCurrentIntTime();
		this.usCreate = 1;
	}
	
	@PreUpdate
	public void preUpdate(){
		this.tsUpdate = getCurrentIntTime();
		this.usUpdate = 1;
	}

	private long getCurrentIntTime() {
		return System.currentTimeMillis();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id.toString();
	}
	
	public long getTsCreate() {
		return tsCreate;
	}
	
	public void setTsCreate(long tsCreate) {
		this.tsCreate = tsCreate;
	}
	
	public long getTsUpdate() {
		return tsUpdate;
	}
	
	public void setTsUpdate(long tsUpdate) {
		this.tsUpdate = tsUpdate;
	}
	
	public long getTsDelete() {
		return tsDelete;
	}

	public void setTsDelete() {
		this.tsDelete = getCurrentIntTime();
	}

	public int getUsCreate() {
		return usCreate;
	}
	
	public void setUsCreate(int usCreate) {
		this.usCreate = usCreate;
	}
	
	public Integer getUsUpdate() {
		return usUpdate;
	}
	
	public void setUsUpdate(Integer usUpdate) {
		this.usUpdate = usUpdate;
	}

	public Integer getUsDelete() {
		return usDelete;
	}

	public void setUsDelete(Integer usDelete) {
		this.usDelete = usDelete;
	}
	
	public String toJson() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(this);
	}

}
