package com.gien.polls.domain.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity(name="questions")
public class QuestionEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@Column(name="questionid")
	private String questionid;
	
	@Column(name="questiontitle")
	private String questiontitle;

	@Column(name="questiondescription")
	private String questiondescription;

	@Column(name="questioncontent")
	private String questioncontent;

	@Column(name="questiontype")
	private int questiontype;

	@Column(name="questionexpired")
	private Date questionexpired;

	@OneToMany(mappedBy = "questions")
	private List<AnswerdEntity> responses;

	public String getQuestionid() {
		return questionid;
	}

	public void setQuestionid(String questionid) {
		this.questionid = questionid;
	}

	public QuestionEntity withQuestionid(String questionid) {
		this.questionid = questionid;
		
		return this;
	}

	public String getQuestiontitle() {
		return questiontitle;
	}

	public void setQuestiontitle(String questiontitle) {
		this.questiontitle = questiontitle;
	}

	public QuestionEntity withQuestiontitle(String questiontitle) {
		this.questiontitle = questiontitle;
		
		return this;
	}

	public String getQuestiondescription() {
		return questiondescription;
	}

	public void setQuestiondescription(String questiondescription) {
		this.questiondescription = questiondescription;
	}

	public QuestionEntity withQuestiondescription(String questiondescription) {
		this.questiondescription = questiondescription;
		
		return this;
	}

	public String getQuestioncontent() {
		return questioncontent;
	}

	public void setQuestioncontent(String questioncontent) {
		this.questioncontent = questioncontent;
	}

	public QuestionEntity withQuestioncontent(String questioncontent) {
		this.questioncontent = questioncontent;
		
		return this;
	}
	
	public int getQuestiontype() {
		return questiontype;
	}

	public void setQuestiontype(int questiontype) {
		this.questiontype = questiontype;
	}

	public QuestionEntity withQuestiontype(int questiontype) {
		this.questiontype = questiontype;
		
		return this;
	}

	public Date getQuestionexpired() {
		return questionexpired;
	}

	public void setQuestionexpired(Date questionexpired) {
		this.questionexpired = questionexpired;
	}

	public QuestionEntity withQuestionexpired(Date questionexpired) {
		this.questionexpired = questionexpired;
		
		return this;
	}

	public List<AnswerdEntity> getResponses() {
		return responses;
	}

	public void setResponses(List<AnswerdEntity> responses) {
		checkResponseEntity();
		
		this.responses = responses;
	}

	public void setResponses(AnswerdEntity responses) {
		checkResponseEntity();
		
		this.responses.add(responses);
	}

	private void checkResponseEntity() {
		if (this.responses == null || this.responses.isEmpty()){
			this.responses = new ArrayList<>();
		}
	}

	@Override
	public String toString() {
		return "com.gien.polls.domain.entities.QuestionEntity {questionid:" + questionid + ", questiontitle:" + questiontitle
				+ ", questiondescription:" + questiondescription + ", questioncontent:" + questioncontent
				+ ", questiontype:" + questiontype + ", questionexpired:" + questionexpired + ", responses:" + responses
				+ "}";
	}
	
}
