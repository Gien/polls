package com.gien.polls.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name="auditoria")
public class AuditableEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@Column(name="objecto", length=500)
	private String object;
	
	@Column(name="tipo", length=50)
	private String type;
	
	@Column(name="accion", length=50)
	private String action;
	
	@Column(name="valores", length=2500)
	private String values;
	
	@Column(name="identificador", length=500)
	private String identify;
	
	@Column(name="cambios", length=2500)
	private String changes;

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public AuditableEntity object(String object) {
		this.object = object;
		
		return this;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public AuditableEntity type(String type) {
		this.type = type;
		
		return this;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public AuditableEntity action(String action) {
		this.action = action;
		
		return this;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public AuditableEntity values(String values) {
		this.values = values;
		
		return this;
	}

	public String getIdentify() {
		return identify;
	}

	public void setIdentify(String identify) {
		this.identify = identify;
	}

	public AuditableEntity identify(String identify) {
		this.identify = identify;
		
		return this;
	}

	public String getChanges() {
		return changes;
	}

	public void setChanges(String changes) {
		this.changes = changes;
	}

	public AuditableEntity changes(String changes) {
		this.changes = changes;
		
		return this;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
