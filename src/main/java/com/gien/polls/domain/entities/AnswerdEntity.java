package com.gien.polls.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="answeres")
public class AnswerdEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@Column(name="responseid")
	private String responseid;
	
	@Column(name="responsecontent")
	private String responsecontent;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "questionid")
	private QuestionEntity questions;

	public String getResponseid() {
		return responseid;
	}

	public void setResponseid(String responseid) {
		this.responseid = responseid;
	}

	public AnswerdEntity withResponseid(String responseid) {
		this.responseid = responseid;
		
		return this;
	}

	public String getResponsecontent() {
		return responsecontent;
	}

	public void setResponsecontent(String responsecontent) {
		this.responsecontent = responsecontent;
	}

	public AnswerdEntity withResponsecontent(String responsecontent) {
		this.responsecontent = responsecontent;
		
		return this;
	}

	public QuestionEntity getQuestions() {
		return questions;
	}

	public void setQuestions(QuestionEntity questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "com.gien.polls.domain.entities.AnswerdEntity {responseid:" + responseid + ", responsecontent:" + responsecontent + ", questions:"
				+ questions + "}";
	}
}
