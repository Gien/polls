package com.gien.polls.domain.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.gien.polls.domain.entities.AuditableEntity;

@Component
public interface IAuditRepository extends JpaRepository<AuditableEntity, UUID>{

}
