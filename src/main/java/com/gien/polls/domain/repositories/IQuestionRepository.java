package com.gien.polls.domain.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.gien.polls.domain.entities.QuestionEntity;

@Component
public interface IQuestionRepository extends JpaRepository<QuestionEntity, UUID>{

    @Query("SELECT p FROM questions p WHERE p.questionid = :questionid")
	QuestionEntity findOneByQuestionid(@Param("questionid") String questionid);

}
